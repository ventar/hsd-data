(function () {
  function b(t, s) {
    this.t = t;
    this.c = t.find(s.move).first();
    this.cs = t.find(s.closed).first();
    this.m = false;
    this.s = false;
    this.hide = 5;
    this.init();
  }
  b.prototype = {
    init: function () {
      var t = this;
      t.box_sizing = t.t.css('box-sizing');
      t.top = t.t.offset().top - $(window).scrollTop();
      t.left = t.t.offset().left - $(window).scrollLeft();
      t.height = t.t.outerHeight();
      t.width = t.t.outerWidth();
      t.w_width = $(window).width();
      t.w_height = $(window).height();
      // if(t.box_sizing != 'border-box'){
      //     t.c_width = t.width - t.t.width();
      //     t.c_height = t.height - t.t.height();
      // }else{
      //     t.c_width = 0;
      //     t.c_height = 0;
      // }
      // t.t.css({'max-height':t.w_height-t.c_height,'max-width':t.w_width-t.c_width,'position':'fixed'});

      t.move();
      // t.size();
    },
    move: function () {
      var t = this;
      t.cs.on('mousedown', function () {
        // t.top = 0;
        // t.left = 0;
        // return false;
      });
      t.c.on('mousedown', function (e) {
        t.m = true;
        t.x = e.pageX;
        t.y = e.pageY;
        t.height = t.t.outerHeight();
        t.width = t.t.outerWidth();
        $(document).on('mousemove', function (e) {
          t.left2 = t.left + e.pageX - t.x;
          t.top2 = t.top + e.pageY - t.y;
          if (t.left2 <= 0) {
            t.left2 = 0;
          } else if (t.left2 >= t.w_width - t.width) {
            t.left2 = t.w_width - t.width;
          }
          if (t.top2 <= 0) {
            t.top2 = 0;
          } else if (t.top2 >= t.w_height - t.height) {
            t.top2 = t.w_height - t.height;
          }
          t.t.css({ 'top': t.top2, 'left': t.left2 });
          return false;
        });
        $(document).on('mouseup', function (e) {
          t.top = t.t.offset().top - $(window).scrollTop();
          t.left = t.t.offset().left - $(window).scrollLeft();

          $(document).off('mousemove');
          $(document).off('mouseup');
          t.m = false;
        });
        return false;

      });

    }
  };
  var y = {
    move: '.title'
  };
  $.fn.bg_move = function (bg) {
    $.extend(y, bg);
    $(this).each(function () {
      new b($(this), y);
    });
  }
})(jQuery);